package ufps.vistas;

import Evaluador.*;

public class TestEvaluador {

    public static void main(String[] args) {

        String url = "https://gitlab.com/madarme/archivos-persistencia/raw/master/expresiones/expresion.txt.txt";
        EvaluadorExpresiones ev = new EvaluadorExpresiones(url);
        
        Expresion e = new Expresion();
        for(Expresion exp: ev.getExpresiones()){
            System.out.println(exp.toString());
            System.out.println(exp.evaluar(exp.toString()));
            
        }
        
        //ex.evaluar(url);
        
        System.out.println(ev.getExpresiones());
    }

}
