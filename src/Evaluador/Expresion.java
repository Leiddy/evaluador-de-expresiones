package Evaluador;

import ufps.util.colecciones_seed.*;

public class Expresion {

    private ListaCD<String> expresiones = new ListaCD();

    public Expresion() {
    }

    public Expresion(String cadena) {

        String v[] = cadena.split(",");
        for (String dato : v) {
            this.expresiones.insertarAlFinal(dato);
        }
    }

    public ListaCD<String> getExpresiones() {
        return expresiones;
    }

    public void setExpresiones(ListaCD<String> expresiones) {
        this.expresiones = expresiones;
    }

    @Override
    public String toString() {
        String msg = "";
        for (String dato : this.expresiones) {
            msg += dato + "";
        }
        return msg;
    }

    public String getPrefijo() {
        return "";
    }

    private static String getPosfijo(String infija) {
        String posfija = "";
        Pila pila = new Pila();

        for (int i = 0; i < infija.length(); i++) {
            char letra = infija.charAt(i);
            if (esOperador(letra)) {
                if (pila.esVacia()) {
                    pila.apilar(letra);
                } else {
                    int pe = prioridadEnExpresion(letra);
                    int pp = prioridadEnPila((char) pila.getTope());
                    if (pe > pp) {
                        pila.apilar(letra);
                    } else {
                        posfija += pila.desapilar();
                        pila.apilar(letra);
                    }
                }
            } else {
                posfija += letra;
            }
        }
        while (!pila.esVacia()) {
            posfija += pila.desapilar();

        }
        return posfija;
    }

    private static double getEvaluarPosfijo(String posfija) {
        Pila pila = new Pila();
        for (int i = 0; i < posfija.length(); i++) {
            char letra = posfija.charAt(i);
            if (!esOperador(letra)) {
                double num = new Double(letra + "");
                pila.apilar(num);
            } else {
                double num2 = (double) pila.desapilar();
                double num1 = (double) pila.desapilar();
                double num3 = operacion(letra, num1, num2);
                pila.apilar(num3);
            }
        }
        return (double) pila.getTope();
    }

    public double evaluar(String infija) {

        String posfija = getPosfijo(infija);
        System.out.println("la expresion posfija es:" + posfija);
        return getEvaluarPosfijo(posfija);

    }

    private static int prioridadEnExpresion(char operador) {

        if (operador == '^') {
            return 4;
        }
        if (operador == '*' || operador == '/') {
            return 2;
        }
        if (operador == '+' || operador == '-') {
            return 1;
        }
        if (operador == '(') {
            return 5;
        }
        return 0;
    }

    private static int prioridadEnPila(char operador) {

        if (operador == '^') {
            return 3;
        }
        if (operador == '*' || operador == '/') {
            return 2;
        }
        if (operador == '+' || operador == '-') {
            return 1;
        }
        if (operador == '(') {
            return 0;
        }
        return 0;
    }

    private static double operacion(char letra, double num1, double num2) {
        if (letra == '*') {
            return num1 * num2;
        }
        if (letra == '/') {
            return num1 / num2;
        }
        if (letra == '+') {
            return num1 + num2;
        }
        if (letra == '-') {
            return num1 - num2;
        }
        if (letra == '^') {
            return Math.pow(num1, num2);
        }
        return 0;
    }

    private static boolean esOperador(char letra) {
        if (letra == '*' || letra == '/' || letra == '+'
                || letra == '-' || letra == '(' || letra == ')' || letra == '^') {
            return true;
        }
        return false;
    }

}
